
import pulumi

from pulumi import Output
from pulumi_aws import elasticloadbalancingv2, get_availability_zones, ec2

#Creating the desired VPC
vpc = ec2.Vpc('VPC',
    cidr_block="10.13.0.0/16",
    enable_dns_hostnames=True,
    enable_dns_support=True, tags={"Name": "AWS-VPC"})

#Creating Internet gatway for private subnet
internet_gateway = ec2.InternetGateway('InternetGway',
    vpc_id=vpc.id,tags={"Name": "InternetGateway-Pulic-Server"})

#getting the avalibility zones (AZs)
zones = Output.from_input(get_availability_zones())
zone_names = zones.apply(
    lambda zs: zs.names)
print(zone_names)
#creating first subnet base on the first AZs
subnet0 = ec2.Subnet("Public-Server",
    vpc_id=vpc.id,
    availability_zone=zone_names.apply(lambda names: names[0]),
    cidr_block="10.13.0.0/24",
    map_public_ip_on_launch=True,tags={"Name": "Subnet-Pulic-Server"})

#creating second subnet base on the second AZ
subnet1 = ec2.Subnet("Private-Server",
    vpc_id=vpc.id,
    availability_zone=zone_names.apply(lambda names: names[1]),
    cidr_block="10.13.1.0/24",
    map_public_ip_on_launch=True, tags={"Name": "Subnet-Private-Server"})

#EIP for NAT gateway && NAT Gateway for internet access in private Subnet
eip = ec2.Eip("Eip-Internet",vpc=True)
NATGway = ec2.NatGateway("Private-Internet",
                         allocation_id=eip.id,
                         subnet_id=subnet0.id,tags={"Name": "Private-NAT"},)

#Route table for public Subnet
route_table0 = ec2.RouteTable('RouteT-Public',
    vpc_id=vpc.id,
    routes=[ec2.RouteTableRouteArgs(
        cidr_block="0.0.0.0/0",
        gateway_id=internet_gateway.id
    )],tags={"Name": "Public-RTable"},)

#Route table for private subnet
route_table1 = ec2.RouteTable('RouteT-Private',
    vpc_id=vpc.id,
    routes=[ec2.RouteTableRouteArgs(
        cidr_block="0.0.0.0/0",
        gateway_id=NATGway.id
    )],tags={"Name": "Private-RTable"},)

#Route table accociates for public subnet
route_table_association0 = ec2.RouteTableAssociation('Public-RouteTable',
    subnet_id=subnet0.id,
    route_table_id=route_table0.id,)

#Route table accociates for private subnet
route_table_association1 = ec2.RouteTableAssociation('Private-RouteTable',
    subnet_id=subnet1.id,
    route_table_id=route_table1.id,)

#public instance security group, port 22 for SSH and 80 for HTTP is set as inbound
group1 = ec2.SecurityGroup(
    "Sg-Public-Server",
    description="Enable access",
    vpc_id=vpc.id,
    ingress=[
        {
            "protocol": "tcp",
            "from_port": 22,
            "to_port": 22,
            "cidr_blocks": ["0.0.0.0/0"],
        },
        {
            "protocol": "tcp",
            "from_port": 80,
            "to_port": 80,
            "cidr_blocks": ["0.0.0.0/0"],
        },
    ],
    egress=[
        {"protocol": "-1", "from_port": 0, "to_port": 0, "cidr_blocks": ["0.0.0.0/0"], }
    ],
    tags={"Name": "Sg-Public-Server"},
)

#private instance security group, port 22 with CIDR of public instance is set
#so only SSH can be done when you are inside public instance
group2 = ec2.SecurityGroup(
    "Sg-Private-Server",
    vpc_id=vpc.id,
    description="Enable access",
    ingress=[
        {
            "protocol": "tcp",
            "from_port": 22,
            "to_port": 22,
            "cidr_blocks": ["10.13.0.0/24"],
        },
{
            "protocol": "tcp",
            "from_port": 0, #port fo ICMP
            "to_port": 65535,
            "cidr_blocks": ["10.13.0.0/24"],
        },
    ],egress=[
        {"protocol": "-1", "from_port": 0, "to_port": 0, "cidr_blocks": ["0.0.0.0/0"], }
    ],tags={"Name": "Sgr-Private-Server"},
)

#Bash for installing apache webserver
user_data = """
        #!/bin/bash
        
        # get admin privileges
        sudo su
        
        # install httpd (Linux 2 version)
        yum update -y
        yum install -y httpd.x86_64
        systemctl start httpd.service
        systemctl enable httpd.service
        echo "Hello World from $(hostname -f)" > /var/www/html/index.html 
        """
# Create the public webserver
server = ec2.Instance(
        "Public-Server",
        instance_type='t2.micro',
        subnet_id=subnet0,
        vpc_security_group_ids=[group1.id],
        user_data=user_data,
        ami='ami-0bd39c806c2335b95',
        key_name='aws_Key',
        tags={"Name": "Public-Server"},
 )
# Create the private webserver Azs
server = ec2.Instance(
    "Private-Server",
    instance_type='t2.micro',
    subnet_id=subnet1,
    vpc_security_group_ids=[group2.id],
    user_data=user_data,
    ami='ami-0bd39c806c2335b95',
    key_name='aws_Key',
    tags={"Name": "Private-Server"},
)


pulumi.export('publicIp', server.public_ip)
pulumi.export('publicHostName', server.public_dns)