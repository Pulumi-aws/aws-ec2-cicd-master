AWS :cloud:, Pulumi :wrench:, Python :snake: and CI/CD :fox:

**Intro**

**VPC structure**:
- Creating a VPC with two subnets with desired CIDR as in **Structure**
- One subnet as public and the othe subnet as private Server.
- Connecting the Public Subnet to Router Table and Internet gateway so the public Subnet has the access to Internet.
- Connecting Private Subnet to Route Table and NAT gateway So the Private subnet can also reach Internet.
- The private subnet can be SSH when you SSH into the public subnet first.
- By using UserData and Bash an apache HTTP Server is installed and run so it shows the Hostname of  that Server. 


                                        **Structure**

![](Structure.PNG)

**Architecture**:
- The code is writen in Pycharm :snake:
- then the code is pusshed into Gitlab and by using CI/CD it is feed into the pipeline.
- initially it installs Pulumi, Python and awscli.
- The the pipeline preview the whole VPC infrastructure.
- if the preview is successful then at the next stage of the pipeline it runs and deploy the VPC on AWS.
                                        **Architecture**

![](architecture.png)


**Notes:** :speech_balloon:

I have used AWS account for the purpuse of this project.

if you want to use your own SSH keys just change the name of the KeyPair in the code to you desired key.

Feel free to run `pulumi destroy --yes` command to delete all of deployed infrastructure.

the whole IaC steps are automatized. Only AWS credentials need to be set in order for the pipeline to run successfully
